using UnityEngine;
using System.Collections;

namespace GameGrater{

public class EnemySpawner : MonoBehaviour {


		public GameObject[] spawnObjects;
		public float spawnTime=2.20f;
		private float repeatRate=1.0f;

	void Start () {
			
			InvokeRepeating ("Spawn", spawnTime, repeatRate);

	}


		void Spawn(){
			

				int spawnObjectsGroup = Random.Range (0, spawnObjects.Length);
					Vector2 spawnPointIndex = new Vector2 (transform.position.x, Random.Range (-2.78f, 2.78f));
				Instantiate (spawnObjects[spawnObjectsGroup], spawnPointIndex, transform.rotation);


		}
	
}
}