using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace GameGrater{



public class GameController : MonoBehaviour {
		Enemy enemy;
		Text scoreText;
		public Text highScoreText;
		public static int highScore;
		public static int score;
		private float[] LevelUpVariable = new float[] {20 ,50 , 70 , 110};




	void Awake(){
			
			scoreText = GetComponent<Text> ();
			score = 0;
			if (!PlayerPrefs.HasKey ("HighScore")) {
				PlayerPrefs.SetInt ("HighScore", 0);

			} 
				
		}
	
	
	void Update () {

			LevelUp ();
			HighScore ();

			scoreText.text = ("Score : " + score);
			highScoreText.text=("High Score : "+ highScore);



		} 

	
		public void HighScore(){

			if (score > PlayerPrefs.GetInt ("HighScore")) {
				PlayerPrefs.SetInt ("HighScore", score);

			} else {

				highScore = PlayerPrefs.GetInt ("HighScore");
			}

			if (PlayerPrefs.HasKey ("HighScore")) {
			
			}
		}

		public void LevelUp(){
			
			if (GameController.score >= LevelUpVariable [0]) {

				Fruits.fruitSpeed = 3.2f;
				Fruits.lifeTime = 10f;
				Enemy.enemySpeed = 3.5f;
				Enemy.lifeTime = 10f;
			} else {
				Fruits.fruitSpeed = 3f;
				Enemy.enemySpeed = 3f;
			}
				if (GameController.score >= LevelUpVariable[1]) {

					Fruits.fruitSpeed = 5f;
					Fruits.lifeTime = 5f;
					Enemy.enemySpeed = 5.2f;
					Enemy.lifeTime = 5f;

			} if (GameController.score >= LevelUpVariable[2]) {
					Fruits.fruitSpeed = 7.01f;
					Fruits.lifeTime = 5f;
					Enemy.enemySpeed = 7.30f;
					Enemy.lifeTime = 5f;

			} if (GameController.score >= LevelUpVariable[3]) {
					Fruits.fruitSpeed = 10f;
					Fruits.lifeTime = 5f;
					Enemy.enemySpeed = 10f;
					Enemy.lifeTime = 5f;


			}
		}

	
			//ABCD
			//aadadasa
}
}