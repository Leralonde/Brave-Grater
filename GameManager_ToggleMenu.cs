﻿using UnityEngine;
using System.Collections;


namespace GameGrater{
	
public class GameManager_ToggleMenu : MonoBehaviour {
		
		private GameManager_Master gameManagerMaster;
		public GameObject menu;

	void Start () {

			ToggleMenu();
	}
	
	void Update () {
	
			CheckForMenuToggleRequest ();
	}

		void OnEnable(){

			SetInitialReferences ();
			gameManagerMaster.GameOverEvent += ToggleMenu;
			
		}

		 void OnDisable(){
			gameManagerMaster.GameOverEvent -= ToggleMenu;
		}

		void SetInitialReferences(){

			gameManagerMaster = GetComponent<GameManager_Master> ();
		}

		public void ToggleMenuButton(){

			ToggleMenu ();

		}


		 void CheckForMenuToggleRequest(){


			if (Input.GetKeyUp (KeyCode.Escape) && !gameManagerMaster.isGameOver) {
				
				ToggleMenu ();
			}



		}

	void ToggleMenu(){

			if (menu != null) {
			
				menu.SetActive (!menu.activeSelf);
				gameManagerMaster.isMenuOn = !gameManagerMaster.isMenuOn;
				gameManagerMaster.CallEventMenuToggle ();

			} else
				Debug.LogWarning ("You nedd to assign GameObject");

		}

	
	

}//class
}//namespace