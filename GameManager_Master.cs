using UnityEngine;
using System.Collections;


namespace GameGrater{
	
public class GameManager_Master : MonoBehaviour {

		public delegate void GameManagerEventHandler();
		public event GameManagerEventHandler MenuToggleEvent;
		public event GameManagerEventHandler GameStartEvent;
		public event GameManagerEventHandler GameOverEvent;
		public event GameManagerEventHandler GameSoundEvent;
	

		public bool isGameOver;
		public bool isMenuOn;


		public void CallEventMenuToggle(){


			if(MenuToggleEvent != null){
				MenuToggleEvent ();
			}

		}



		public void CallEventGameStart(){
			if (GameStartEvent != null) {
				GameStartEvent ();
			}

		}

		public void CallEventGameOver(){
			if (GameOverEvent != null) {
			//	isGameOver = true;
				GameOverEvent ();
			}

		}

		public void CallEventGameSound(){
			if (GameSoundEvent != null) {
			
				GameSoundEvent ();
			}

		}

} // class
} // namespace