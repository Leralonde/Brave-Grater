﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace GameGrater{

public class PlayerController : MonoBehaviour {

	bool currentPlatformAndroid=false;
	private float speed=5f;
	Vector3 position;
	Rigidbody2D rb;
		public Text timerText;
		private float timer=3.0f;

	GameManager_Master gameManagerMaster;

	public Button button;
	public AudioSource audioDead;



		void Awake(){
				
			rb = GetComponent<Rigidbody2D> ();
			#if UNITY_ANDROID
			currentPlatformAndroid=true;
			#else
			currentPlatformAndroid=false;
			#endif
		

		}


		void Start () {

			rb.gravityScale = 0;
			speed = 0;


			StartCoroutine (IsGameStart ());

			position = transform.position;
			if (currentPlatformAndroid) {
				Debug.Log ("Android");

			} else
				Debug.Log ("Windows");
		}

		void Update(){

			GameCountDown ();


			if (currentPlatformAndroid == true) {

			} else {
				position.x += Input.GetAxis ("Horizontal") * speed * Time.deltaTime;
				position.x = Mathf.Clamp (position.x, -2.99f, 2.01f);
				position.y += Input.GetAxis ("Vertical") * speed * Time.deltaTime;
				transform.position = position;


			}
			position = transform.position;
			position.x = Mathf.Clamp (position.x, -2.99f, 2.01f);
			transform.position = position;
		}

		//Count Down IE
		IEnumerator IsGameStart(float waitTime=3.0f){
			yield return new WaitForSeconds (waitTime);
			rb.gravityScale = 0.5f;
			speed = 5.0f;

			if (timerText.text == "Go") {
				timerText.gameObject.SetActive(false);

			}

		}
		 
		//Count Down
		public void  GameCountDown(){
		
			timer -= Time.deltaTime;
			timerText.text = timer.ToString ("0");
			if (timer <= 1) {
				timerText.text = "Go";
			}
		
		}

	
	
	
	// Player Crashing
	public void OnCollisionEnter2D(Collision2D col){

			if (col.gameObject.tag == "BlockMountain" || col.gameObject.tag=="Enemy") {

			// GameOver
			gameManagerMaster = GameObject.Find ("GameManager").GetComponent<GameManager_Master> ();	
			gameManagerMaster.CallEventGameOver ();
			// GameOver

			

			audioDead.Play ();

			Destroy (gameObject);


			
		}



	}



		// Move move move

		public void MoveForward(){
			rb.velocity = new Vector2 (speed, 0);

		}


		public void MoveBack(){

			rb.velocity = new Vector2 (-speed, 0);
				

		}
		public void MoveUp(){
		
			rb.velocity = new Vector2 (0, speed);
		}
			

		public void SetVelocityZero(){

			rb.velocity = Vector2.zero;
		}
			
	
	}  // class

}// namespace