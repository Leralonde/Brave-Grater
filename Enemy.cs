using UnityEngine;
using System.Collections;

namespace GameGrater{

public class Enemy : MonoBehaviour {
		
		public static float enemySpeed=2f;
		public static float lifeTime=10f;

	void Start () {

			//destroy gameobjects with timer
			Destroy (gameObject,lifeTime);


	}
	
	void Update () {

			transform.Translate (new Vector3 (1, 0,0) * -enemySpeed * Time.deltaTime);
				

	}

	
	
}
}