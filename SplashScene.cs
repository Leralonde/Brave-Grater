using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SplashScene : MonoBehaviour {



	public float delayTime=3.0f;

	IEnumerator Start(){

		yield return new WaitForSeconds (delayTime);

		SceneManager.LoadScene("MainMenu");
	}
}
