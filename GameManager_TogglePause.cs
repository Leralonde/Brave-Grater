﻿using UnityEngine;
using System.Collections;


namespace GameGrater{
public class GameManager_TogglePause : MonoBehaviour {

		private GameManager_Master gameManagerMaster;
		private bool isPaused;
		public Canvas canvas;

		void OnEnable(){
			
			SetInitialReferences();
			gameManagerMaster.MenuToggleEvent += TogglePause;
		}

		void OnDisable(){
			
			gameManagerMaster.MenuToggleEvent -=TogglePause;

		}

		
		 void TogglePause(){
			
			if (isPaused) {
				Time.timeScale = 0;
				canvas.gameObject.SetActive (false); // disable settings canvas
				isPaused = false;

			} else {
				Time.timeScale = 1;
				isPaused = true;
				canvas.gameObject.SetActive (true);

			}
		}

	

		void SetInitialReferences(){

			gameManagerMaster = GetComponent<GameManager_Master> ();
		}
			
}
}